## DiseaseOntology##
###countDiseaseOntologyTerms.sparql###

~~~sparql
# ENDPOINT <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
# This SPARQL Query extracts all Disease Ontology terms with proper references
# to the original source as they are suppose to be added

PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX v: <http://www.wikidata.org/prop/statement/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX reference: <http://www.wikidata.org/prop/reference/>
SELECT DISTINCT ?doidValue WHERE {
    ?disease wdt:P279 wd:Q12136 ;
        p:P699 ?doid ;
        wdt:P699 ?doidValue .
    ?doid prov:wasDerivedFrom ?derivedFrom .
    ?derivedFrom reference:P248 ?version ;
                 reference:P143 ?importedFrom ;
                 reference:P813 ?dateRetrieved .
}
~~~
###countRanks.sparql###

~~~sparql
# ENDPOINT <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
# This SPARQL Query extracts all Disease Ontology terms and count the number of ranks  
# for each Disease ontology term

PREFIX wd: <http://www.wikidata.org/entity/> 
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX reference: <http://www.wikidata.org/prop/reference/>

SELECT DISTINCT ?doVersion ?rank (count(?rank) as ?counts)  WHERE {
   VALUES ?rank { wikibase:DeprecatedRank wikibase:NormalRank }
   ?diseases p:P699 ?doid .
   ?doid wikibase:rank ?rank .
   ?doid prov:wasDerivedFrom ?derivedFrom .
   ?derivedFrom reference:P248 ?doRelease .
   ?doRelease rdfs:label ?doVersion .
   FILTER (LANG(?doVersion) = 'en')
}
GROUP BY ?doVersion ?rank
~~~
###DOwithIncorrectReferences.sparql###

~~~sparql
# ENDPOINT <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
# This SPARQL Query extracts all Disease Ontology terms with incorrect or missing references
# to the original source as they are suppose to be added by ProteinBoxBot

PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX v: <http://www.wikidata.org/prop/statement/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX reference: <http://www.wikidata.org/prop/reference/>
SELECT DISTINCT ?disease ?doidValue WHERE {
    {
        ?disease wdt:P279 wd:Q12136 ;
                 p:P699 ?doid ;
                 wdt:P699 ?doidValue .
    } MINUS {
        ?doid prov:wasDerivedFrom ?derivedFrom .
        ?derivedFrom reference:P248 ?version ;
             reference:P143 ?importedFrom ;
             reference:P813 ?dateRetrieved .
  }
}
~~~
--------------