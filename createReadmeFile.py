__author__ = 'andra'

import os
rootdir = '.'
readme = open("README.md", 'w')
for subdir, dirs, files in os.walk(rootdir):
    writeDir = False
    for bestand in files:
        if bestand.endswith(".sparql"):
            writeDir = True
            break
    if writeDir:
        readme.write("## "+subdir.replace("./", "")+"##\n ")
    for file in files:
        if ".sparql" in file:
            file_content = open(os.path.join(subdir, file), 'r')
            readme.write("###"+file+"###\n")
            readme.write("\n~~~sparql\n")
            readme.write(file_content.read()+'\n')
            readme.write("~~~\n")



